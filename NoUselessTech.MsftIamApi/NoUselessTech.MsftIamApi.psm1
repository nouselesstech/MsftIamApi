# Get public and private function definition files.
$Public  = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1)

#Dot source the files
ForEach($import in $Public) {
    Try {
        . $import.fullname
    } Catch {
        throw "Failed to import function $($import.fullname): $_"
    }
}


Export-ModuleMember -Function $Public.Basename

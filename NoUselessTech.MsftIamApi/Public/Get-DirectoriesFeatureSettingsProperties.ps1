Function Get-DirectoriesFeatureSettingsProperties {
    <#
        .SYNOPSIS
        Returns an object of directory feature setting properties. 

        .DESCRIPTION
        I don't know if there's a lot of value for most users, but it's here if you want it.

        .LINK
        https://github.com/nouselesstech/MsftIamApi
    #>

    try {
        if ($Null -eq $Global:IamToken) {
            throw "Authenticate with Connect-MsftIamApi before running this command."
        }

        $Response = $Null

        ## Prepare Request
        $Headers = @{}
        $Headers.Authorization              = "Bearer $($Global:IamToken.access_token)"
        $Headers.'Content-Type'             = 'application/json'
        $Headers.'Accept'                   = '*/*'
        $Headers.'User-Agent'               = 'NoUselessTech.IamFetcher'
        $Headers.'X-Ms-Client-Request-Id'   = (New-Guid).Guid
        $Method = 'GET'

        ## Send the Request
        $Response = (Invoke-WebRequest `
                        -Uri "https://main.iam.ad.ext.azure.com/api/Directories/FeatureSettingsProperties" `
                        -Headers $Headers `
                        -Method $Method).Content | ConvertFrom-Json -Depth 100
        
        return $Response

    } catch {
        throw "Could not get Feature Settings Properties. $_"
    }
}
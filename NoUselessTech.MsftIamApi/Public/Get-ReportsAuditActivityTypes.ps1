Function Get-ReportsAuditActivityTypes {
    <#
        .SYNOPSIS
        Returns all audit log activity types. 

        .DESCRIPTION
        This is useful if you're trying to map out potential events in azure. 

        .LINK
        https://github.com/nouselesstech/MsftIamApi
    #>
    try {
        if ($Null -eq $Global:IamToken) {
            throw "Authenticate with Connect-MsftIamApi before running this command."
        }

        $Response = $Null

        ## Prepare Request
        $Headers = @{}
        $Headers.Authorization              = "Bearer $($Global:IamToken.access_token)"
        $Headers.'Content-Type'             = 'application/json'
        $Headers.'Accept'                   = '*/*'
        $Headers.'User-Agent'               = 'NoUselessTech.IamFetcher'
        $Headers.'X-Ms-Client-Request-Id'   = (New-Guid).Guid
        $Method = 'GET'

        ## Send the Request
        $Response = (Invoke-WebRequest `
                        -Uri "https://main.iam.ad.ext.azure.com/api/Reports/AuditActivityTypesV2" `
                        -Headers $Headers `
                        -Method $Method).Content | ConvertFrom-Json -Depth 100
        
        return $Response

    } catch {
        throw "Could not get Audit Activity Types. $_"
    }
}